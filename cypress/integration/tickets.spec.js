describe("Tickets", () =>{
    beforeEach (() => cy.visit("https://ticket-box.s3.eu-central-1.amazonaws.com/index.html"));

    it("fills all the text input fields", () =>{
        //declarando a variavel
        const firstName = "Erick";
        const lastName = "Sousa";

        //o comando get e para identificar o elemento da pagina
        //e o type para enviar uma informacao, no caso do codigo abaixo
        //serve para pegar o texto declarado da variavel
        //e colocar no campo do first name
        cy.get("#first-name").type(firstName);
        cy.get("#last-name").type(lastName);
        cy.get("#email").type("talkingabouttesting@gmail.com");
        cy.get("#requests").type("Vegetarian");
        cy.get("#signature").type(`${firstName} ${lastName}`);
    });

    it("select two tickets", () => {
        //o comando select serve para abrir a caixa de opcoes
        //e selecionar o valor ou a opcao que voce quer
        cy.get("#ticket-quantity").select("2");
    });

    it("select 'vip' ticket type", () => {
        //o comando check serve para selecionar a combo-box que você escolhe
        //assim, declarado pelo objeto mapeado no get
        cy.get("#vip").check();
    });

    it("selects 'social media' checkbox",() => {
        cy.get("#social-media").check();
    });

    it("selects 'friends', and 'publication, then uncheck 'friend'", () => {
        cy.get("#friend").check();
        cy.get("#publication").check();
        //o comando uncheck serve para retirar a selecao da combo-box
        cy.get("#friend").uncheck();
    });

    it("has 'TICKETBOX' header's heading",() => {
        //o comando should serve para verificar se existe algo
        //para ser validado, assim, declarando o contain, verifica se
        //o texto declarado na frente existe e sera validado
        cy.get("header h1").should("contain", "TICKETBOX");
    });

    it("alerts on invalid email", () => {
        //o comando as serve para apelidar o objeto declarado, facilitando
        //a utilizacao do mesmo para outras partes da logica
        //e o type para escrever dentro do objeto apelidado
        cy.get("#email")
            .as("email")
            .type("talkingabouttesting-gmail.com");

        //outra forma do comando should validando se o objeto existente
        //esta sendo apresentado na tela, no caso do email invalido, sera
        //alterado o campo para vermelho e sera validado se foi alterado
        cy.get("#email.invalid").should("exist")

        cy.get("@email")
            .clear()
            .type("talkingabouttesting@gmail.com");

        //outra forma do comando should, validando se o mesmo que
        //um momento do teste foi alterado, agora validando se nao esta
        //sendo apresentado na tela, atraves do not.exist
        cy.get("#email.invalid").should("not.exist");
    });

    it("fill and reset the form", () => {
        const firstName = "Erick";
        const lastName = "Sousa";
        const fullName = `${firstName} ${lastName}`;

        cy.get("#first-name").type(firstName);
        cy.get("#last-name").type(lastName);
        cy.get("#email").type("talkingabouttesting@gmail.com");
        cy.get("#ticket-quantity").select("2");
        cy.get("#vip").check();
        cy.get("#friend").check();
        cy.get("#requests").type("IPA beer");

        //no comando get, e informado para buscar o elemento dentro do paragrafo, do tipo agreement
        //que contenha o seguinte texto, declarado com a variavel (nome do cliente)
        //seja validado a alteracao da mensagem
        cy.get(".agreement p").should(
            "contain", 
            `I, ${fullName}, wish to buy 2 VIP tickets.`);

        //comando get busca o checkbox com o elemento de id agree
        //para ser selecionado
        cy.get("#agree").click();
        cy.get("#signature").type(fullName);

        //o comando get mapeia o objeto botao que tem o comando de submit
        //fazendo uma verificacao do mesmo se ele nao esta desabilitado
        cy.get("button[type='submit']")
            .as("submitButton")
            .should("not.be.disabled");

        //assim, seguindo o comentario acima, clicando no objeto mapeado
        //o botao de reset, fazendo limpar todos os dados da aplicacao
        cy.get("button[type='reset']").click();

        //e novamente, seguindo a logica dos metodos acima, validando o botao desabilitado
        cy.get("@submitButton").should("be.disabled");
    });

    it("fills mandatory fields using support command", () => {
        const customer = {
            firstName: "João",
            lastName: "Silva",
            email: "joaosilva@example.com"
        };

        cy.fillMandatoryFields(customer);

        cy.get("button[type='submit']")
            .as("submitButton")
            .should("not.be.disabled");

        cy.get("#agree").uncheck();

        cy.get("@submitButton").should("be.disabled");
    });

});
